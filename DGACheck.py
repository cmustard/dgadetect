#!/usr/bin/env python
# _*_coding:utf-8

import pickle
import sys
import os.path
from re import findall
from base64 import b64decode
from sklearn.externals import joblib

try:
	from . import scalerSerial
except ValueError:
	import scalerSerial  # 如果直接运行这个文件



def getNumProportion(domain):
	"""
	数字占域名的比例
	:param domain:
	:return:
	"""
	count = len(findall(r'[1234567890]', domain.lower()))
	# proportion = (count + 0.0) / len(set(domain))
	return count


def getDomainLength(domain):
	"""
	得到域名长度
	:param domain:
	:return:
	"""
	return len(domain)


def getVowelProportion(domain):
	"""
	元音比例
	:param domain:
	:return:
	"""
	count = len(findall(r"[aeiou]", domain.lower()))
	# proportion = (count + 0.0) / len(domain)
	return count


def getDomainType(domain):
	"""
	域名类型
	:param domain:
	:return:
	"""
	_types = domain.split(".")[-1]
	output = 0
	for _char in _types:
		output += ord(_char)
	return output * 0.01


def getDeduplicationDomainLength(domain):
	"""
	去重后域名长度
	:param domain:
	:return:
	"""
	return len(set(domain))


def getNumCharExchangeRatio(domain):
	"""
	数字字母交换比例
	:param domain:
	:return:
	"""
	count = len(findall(r"[a-z]{1}\d{1}", domain.lower()))
	return count


def DGACheck(domain, modelfile):
	"""
	检测域名是否为DGA生成域名
	:param domain:
	:param modelfile  模型文件路径
	:return: 0 or 1
	"""
	try:
		if not os.path.exists(modelfile):
			print("modelfile not exists!!!")
			return
		domains = domain.split(".")
		domain = ".".join(domains[-2:])
		features = [[getNumCharExchangeRatio(domain), getDomainLength(domain),
		             getDomainType(domain), getNumProportion(domain), getVowelProportion(domain),
		             getDeduplicationDomainLength(domain)]]
		# print(scalerSerial.MIN_MAX_SCALER)
		min_max_scaler = pickle.loads(b64decode(scalerSerial.MIN_MAX_SCALER),)
		features = min_max_scaler.transform(features)
		# print(features)
		clf = joblib.load(modelfile)
		output = clf.predict(features)
		return output[0]
	except Exception as e:
		# print(e)
		return


if __name__ == '__main__':
	# 100个dga域名
	domains = ['57fo5rqjmsgfbgmo3x.cn', 'hdmtinalcentricem.com', 'vnnxfxqdpqrs.pw', 'yjh21a16i5wct1uoimjh1tqdk87.net',
	           'jprjanerraticallyqozaw.com', 'iwoiiologistbikerepil.com', 'kojhrasildeafeninguvuc.com', 'jevogad.info',
	           'jzjmvkn.com', 'prjqsatformalisticirekb.com', 'xtmrwckudosf.pw', 'zfuwsatformalisticirekb.com',
	           'ecywamentalistfanchonut.com', 'brzuanerraticallyqozaw.com', 'zxtlancorml.com', 'erluxmkchbdooouh.eu',
	           'ejblxccrkxvdutrj.eu', 'sqdgpartbulkyf.com', 'xkcffuyyylfe.su', 'vakmj82lhfgpa72fij.biz',
	           'jxedsikathrinezad.com', 'bjsafordlinnetavox.com', 'weoneo.com', 'hthmppwwnegllumr.eu',
	           'l1f2opyvdozfok5txb.net', 'ywhn1oe2vafifmlq6j.cn', 'bdtnvinskycattederifg.com', 'qxewfkmufmy.com',
	           'chjdfaifmscbsuba.eu', 'wkfrllaabettingk.com', 'kixyrsensinaix.com', 'ousamachuslazaroqok.com',
	           'tnmdsatformalisticirekb.com', 'gcsygiuchkvudixc.eu', 'dvyualitydevonianizuwb.com',
	           'pjrjorcajanunal.com', 'bvqnestnessbiophysicalohax.com', 'nbxiemexoryrfbdj.eu',
	           'eclxamentalistfanchonut.com', 'rmryoabfysqpiuya.eu', 'guawamentalistfanchonut.com', 'yhtxxopvejqu.biz',
	           'mujlidablyhoosieraw.com', 'eeaosatformalisticirekb.com', 'ielumk.biz', 'yli445otv87lbpc7fe.net',
	           'flvqjdhhmtwg3p2u3u.net', 'lpbmleasuredehydratorysagp.com', 'ohltbtmvfwqneokq.eu', 'eorjwmtclalcjvge.eu',
	           'gwwysikathrinezad.com', 'kocofiiugkeq.net', 'hqbstujxpokm.pw', 'fpaqenhancedysb.com',
	           'fodliqwkbwfetshm.eu', 'gylkd1y7ve7zihu315.ru', '16fl7ri1xam7fh19ew6ou16qwxe5.biz', 'myizanarianaqh.com',
	           'h85wc4iw3t13jar27i.biz', 'gyrfsatformalisticirekb.com', 'ss5txp2adlacs2sl75.ru',
	           'kocysatformalisticirekb.com', 'suggvinskycattederifg.com', 'ettwkgkgfwtfpyis.eu',
	           'x8rmrvenbcd4bfhm8r.cn', 'jvobardenslavetusul.com', 'oyzrvinskycattederifg.com',
	           'htcfleasuredehydratorysagp.com', 'yyheleasuredehydratorysagp.com', 'susoanerraticallyqozaw.com',
	           'ukulyl.com', 'kaphgsuiwcymao.biz', 'ipthgopojyxnvpkw.eu', 'bnmiererwyatanb.com',
	           'u4ff86962f3ef1118ee56d441d4e45ce0a.cc', 'sungiologistbikerepil.com', 'bjdeardenslavetusul.com',
	           'pcbcdmqhaonlexus.eu', 'xroriologistbikerepil.com', 'asfbqjahebmqbkqe.eu', 'ryxymfuljpfpiavs.eu',
	           'rn265s3okghqm3cnyk.cn', 'kyjdgvfkufvquihv.eu', 'wcvdererwyatanb.com', 'maryvonnemaddison.net',
	           'astfererwyatanb.com', 'qudhqyddnttitxsj.eu', 'eidusatformalisticirekb.com', 'tueleiemukkrxepu6g.net',
	           'qxolupkhiovf.cc', 'ntqpllaabettingk.com', 'vt64jfzrctymso6yha.biz', 'ehavqigaharii.ddns.net',
	           'rrplllaabettingk.com', 'tjcgancorml.com', 'jbjhancorml.com', 'ztaoalitydevonianizuwb.com',
	           'yc2cc6oeq6o3flo81h.ru', 'nrklmachuslazaroqok.com', 'emtvalitydevonianizuwb.com', "twitter.com",
	           "google.co",
	           "jd.com",
	           "sohu.com",
	           "live.com",
	           "weibo.com",
	           "sina.com.cn",
	           "instagram.com",
	           "google.co.jp",
	           "login.tmall.com",
	           "360.cn"]
	
	modelfile = "dga-svm-py2-model-10.m"
	dga = 0
	for domain in domains:
		print(domain)
		ret = DGACheck(domain, modelfile)
		print(ret)
		if ret:
			dga += 1
	print("==>dga ", dga)
