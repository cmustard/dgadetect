# DGADetect

使用机器学习检测DGA算法生成域名
支持python3与python2


## 样本训练模型生成
脚本使用帮助
```python
"Usage:
	python .py normal_file.csv malicious_file.csv svm|nb  [Modelfile.m]
```

```python
Python  DGATrain.py   alex-test-10.csv    dga-test-10.csv    svm    [dga-svm-model-10.m]
```

## 域名检测
```python
def DGACheck(domain, modelfile):
	"""
	检测域名是否为DGA生成域名
	:param domain:
	:param modelfile  模型文件路径
	:return: 0 or 1
	"""
```

## SVM算法调参
以下调试样本数量为8w，正常域名与DGA域名各一半

```python
clf = svm.SVC(kernel="rbf", C=1,gamma=1).fit(x_train, y_train)
```
三折交叉验证测试结果如下
```python
('==>', array([0.88748919, 0.87653717, 0.869956  ]))
('average score is :\n', 0.8779941210888675)
```

```python
clf = svm.SVC(kernel="rbf", C=2,gamma=1).fit(x_train, y_train)
```
三折交叉验证测试结果如下
```python
('==>', array([0.88805325, 0.87766538, 0.87168591]))
('average score is :\n', 0.8791348437880714)
```


```python
clf = svm.SVC(kernel="rbf", C=2,gamma=1.0/5).fit(x_train, y_train)
```
三折交叉验证测试结果如下
```python
('==>', array([0.8827511 , 0.87435599, 0.86811327]))
('average score is :\n', 0.8750734521429578)
```