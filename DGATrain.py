#!/usr/bin/env python
# _*_coding:utf-8

import sys
import os
import csv
import pickle
import time
from re import findall
from base64 import b64encode
import numpy as np
from sklearn import svm, preprocessing
from sklearn.model_selection import cross_val_score
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.externals import joblib
from sklearn.naive_bayes import GaussianNB

"""
特征提取
1.域名长度
2.去重后数字占域名的比例
3.元音辅音个数的比例
4.域名类型
5.域名去重后的长度
6.数字字母交换比例
"""

MIN_LEN = 6
normal_sample_file = "alex-test.csv"
malicious_sample_file = "dga-test.csv"
model_file = None


def loadSampleFile(filename):
	"""
	加载样本文件
	:param filename:
	:return:
	"""
	domain_list = []
	csv_reader = csv.reader(open(filename))
	for row in csv_reader:
		domain = row[1]
		progressBar()
		if len(domain) >= MIN_LEN:
			domain_list.append(domain)
	return domain_list


def progressBar():
	sys.stdout.write(" " * 5 + "\r")
	sys.stdout.flush()
	sys.stdout.write("load file..." + "\r")
	sys.stdout.flush()


def getNumProportion(domain):
	"""
	数字占域名的比例
	:param domain:
	:return:
	"""
	count = len(findall(r'[1234567890]', domain.lower()))
	# proportion = (count + 0.0) / len(set(domain))
	return count


def getDomainLength(domain):
	"""
	得到域名长度
	:param domain:
	:return:
	"""
	return len(domain)


def getVowelProportion(domain):
	"""
	元音比例
	:param domain:
	:return:
	"""
	count = len(findall(r"[aeiou]", domain.lower()))
	proportion = (count + 0.0) / len(domain)
	return count


def getDomainType(domain):
	"""
	域名类型
	:param domain:
	:return:
	"""
	_types = domain.split(".")[-1]
	output = 0
	for _char in _types:
		output += ord(_char)
	return output * 0.01


def getDeduplicationDomainLength(domain):
	"""
	去重后域名长度
	:param domain:
	:return:
	"""
	return len(set(domain))


def getNumCharExchangeRatio(domain):
	"""
	数字字母交换比例
	:param domain:
	:return:
	"""
	count = len(findall(r"[a-z]{1}\d{1}", domain.lower()))
	return count


def getFeature(malicious_sample_file, normal_sample_file):
	"""

	:param malicious_sample:
	:param normal_sample:
	:return:
	"""
	malicious_domin_list = loadSampleFile(malicious_sample_file)
	normal_domain_list = loadSampleFile(normal_sample_file)
	print("==> Sample file loading completed!!!")
	print("==> Start loading features!!!")
	features = []
	for domain in (normal_domain_list + malicious_domin_list):
		temp = [getNumCharExchangeRatio(domain), getDomainLength(domain),
		        getDomainType(domain), getNumProportion(domain), getVowelProportion(domain),
		        getDeduplicationDomainLength(domain)]
		features.append(temp)
	print("==> Feature loading completed!!!")
	sample_output = [0] * len(normal_domain_list) + [1] * len(malicious_domin_list)
	# 特征标准化
	print("==> Feature standardization!!!")
	min_max_scaler = preprocessing.MinMaxScaler()
	features = min_max_scaler.fit_transform(features)
	pickle_scaler = pickle.dumps(min_max_scaler, protocol=2)
	if sys.version[0] == '2':
		with open("scalerSerial.py", "wb") as f:
			content = """#!/usr/bin/env python
# _*_coding:utf-8_*_

MIN_MAX_SCALER = \"{}\"""".format(b64encode(pickle_scaler))
			f.write(content)
	else:
		with open("scalerSerial.py", "wb") as f:
			content = """#!/usr/bin/env python
# _*_coding:utf-8_*_

MIN_MAX_SCALER = {}""".format(b64encode(pickle_scaler)).encode()
			f.write(content)
		
	print("==> Standardized completion!!!")
	return features, sample_output


def NBTrain():
	"""
	使用朴素贝叶斯算法训练模型
	:return:
	"""
	normal_domain_list = loadSampleFile(normal_sample_file)
	malicious_domain_list = loadSampleFile(malicious_sample_file)
	print("==> Sample file loading completed!!!")
	# 打标记
	print("==> Start NB trainning!!!")
	
	domain_list = np.concatenate((normal_domain_list, malicious_domain_list))
	normal_sample_output = [0] * len(normal_domain_list)
	malicious_sample_output = [1] * len(malicious_domain_list)
	sample_output = np.concatenate((normal_sample_output, malicious_sample_output))
	print("==> 2-gram Split domain!!!")
	# 以2-gram算法分割域名
	cv = CountVectorizer(ngram_range=(2, 2),
	                     token_pattern=r'\w',
	                     decode_error='ignore',
	                     strip_accents='ascii',
	                     max_features=10000,
	                     stop_words='english',
	                     max_df=1.0,
	                     min_df=1)
	# 标准化
	sample_input = cv.fit_transform(domain_list).toarray()
	
	# 实例化NB算法
	clf = GaussianNB()
	clf.fit(sample_input, sample_output)
	print("==>Training completed!!!")
	
	joblib.dump(clf, model_file)
	# 十折交叉验证
	print("==> Start verification test!!!")
	score = cross_val_score(clf, sample_input, sample_output, n_jobs=-1, cv=10)
	print("==>score:", score)
	print("average score is :", np.mean(score))
	print("==> Finished!!!")


def loadSVM(x_train, y_train, x_test=None, y_test=None, isTest=False):
	"""
	使用SVM训练模型
	:param x_train:
	:param y_train:
	:param x_test:
	:param y_test:
	:return:
	"""
	if x_test is not None and y_test is not None:
		x_train = x_train + x_test
		y_train = y_train + y_test
	print("==> Start SVM trainning!!!")
	clf = svm.SVC(kernel="rbf", C=3,gamma=1.0/4).fit(x_train, y_train)
	if model_file is not None:
		joblib.dump(clf, model_file)
	print("==>Training completed!!!")
	if isTest:
		print("-" * 50)
		print("==> Start verification test!!!")
		# 十次交叉验证
		score = cross_val_score(clf, x_train, y_train, n_jobs=-1, cv=3)
		print("==>", score)
		print("average score is :\n", np.mean(score))
	print("==> Finished!!!")


def SVMtrain():
	"""
	svm训练
	:return:
	"""
	x, y = getFeature(malicious_sample_file, normal_sample_file)
	loadSVM(x, y, isTest=True)


def main():
	global normal_sample_file, malicious_sample_file, model_file
	if len(sys.argv) < 2:
		print("""Usage:
		python .py normal_file.csv malicious_file.csv svm|nb  [Modelfile.m]""")
		exit(0)
	normal_file = sys.argv[1]
	malicious_file = sys.argv[2]
	algo = sys.argv[3].lower().strip()
	# print(os.path.exists(normal_file),os.path.exists(malicious_file))
	if not os.path.exists(normal_file) or not os.path.exists(malicious_file):
		print("File does not exist")
		exit(0)
	normal_sample_file = normal_file
	malicious_sample_file = malicious_file
	if len(sys.argv) == 5:
		model = sys.argv[4]
		model_file = model
	
	if algo != "nb" and algo != "svm":
		print("The parameter is incorrect!!!")
		exit(0)
	# print(sys.argv)
	if algo == 'nb':
		NBTrain()
	if algo == 'svm':
		SVMtrain()


if __name__ == '__main__':
	# getNumCharExchangeRatio("as34sdd3aas1d.com")
	start = time.time()
	main()
	print("==> spend time is :{}".format(time.time() - start))
